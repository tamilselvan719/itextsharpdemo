﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace ITextSharpDemo
{
    class Program_bk
    {
        static void Main_bk(string[] args)
        {
            var AccountList = new [] 
            { 
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
                new {Name = "Tamil", Role = "Admin", Email = "tamilselvan719@gmail"},
            };
            PdfPTable table = new PdfPTable(3);

            iTextSharp.text.Font tableHeaderFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.COURIER, 12f, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.WHITE);

            //Creating table header
            PdfPCell tableCell = new PdfPCell();
            tableCell.BackgroundColor = BaseColor.BLACK;

            tableCell.Phrase = new Phrase("Name", tableHeaderFont);
            table.AddCell(tableCell);

            tableCell.Phrase = new Phrase("Role", tableHeaderFont);
            table.AddCell(tableCell);

            tableCell.Phrase = new Phrase("Email", tableHeaderFont);
            table.AddCell(tableCell);

            foreach (var x in AccountList)
            {
                table.AddCell(x.Name.ToString());
                table.AddCell(x.Role.ToString());
                table.AddCell(x.Email.ToString());
            }

            System.IO.MemoryStream ms = PdfGenerator.CreatePDF(table);

            FileStream fs = new FileStream("doc.pdf", FileMode.Create, FileAccess.Write);
            MemoryStream msRes = new MemoryStream(ms.ToArray());
            msRes.WriteTo(fs);
            fs.Close();
            msRes.Close();
            Console.WriteLine("Press any key...");            
        }
    }
}